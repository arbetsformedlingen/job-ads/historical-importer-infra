apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: historical-importer-pipeline
spec:
  params:
    - name: SKIP_S3_UPLOAD
      description: >-
        If set to true, the pipeline will skip the S3 upload step.
      default: "false"

  tasks:
    # Generate the dates and year values required by later tasks
    - name: get-yesterdays-date-and-year
      taskSpec:
        results:
          - name: date
            description: >-
              Date to process. Get yesterday.
          - name: year
            description: >-
              Year to process. Taken from yesterdays date.
          - name: date_today
            description: >-
              Todays date.
        steps:
          - image: >-
              docker-images.jobtechdev.se/script-container/script-container:latest
            imagePullPolicy: Always
            name: get-date
            script: |
              date --date="1 day ago"  +%Y-%m-%d | tr -d '\n' > $(results.date.path)
              date --date="1 day ago"  +%Y | tr -d '\n' > $(results.year.path)
              date --date="today"  +%Y-%m-%d | tr -d '\n' > $(results.date_today.path)
              echo "$results.date"
              echo "$results.year"
              echo "$results.date_today"

    # Fetches all ads that changed yesterday from JobStream, anonymizes them and adds some fields to them.
    - name: stream
      taskRef:
        kind: Task
        name: stream
      params:
        - name: source
          value: $(tasks.get-yesterdays-date-and-year.results.date)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - get-yesterdays-date-and-year

    # Filters the ads (keeps the ones marked for archival) and updates some fields before saving them to json and jsonl files.
    - name: export-ads
      taskRef:
        kind: Task
        name: export-ads
      params:
        - name: yesterday
          value: $(tasks.get-yesterdays-date-and-year.results.date)
        - name: today
          value: $(tasks.get-yesterdays-date-and-year.results.date_today)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - stream

    # Rename files to yesterday's date (from today's date).
    - name: rename-files
      taskRef:
        kind: Task
        name: rename-files
      params:
        - name: from
          value: $(tasks.get-yesterdays-date-and-year.results.date_today)
        - name: to
          value: $(tasks.get-yesterdays-date-and-year.results.date)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - export-ads

    # Compress (zip) the json and jsonl files and upload them to Minio. Can be skipped by setting the SKIP_S3_UPLOAD parameter to true.
    - name: s3-upload
      taskRef:
        kind: Task
        name: s3-upload
      when:
        - input: $(params.SKIP_S3_UPLOAD)
          operator: notin
          values:
            - "true"
      params:
        - name: destination
          value: annonser/historiska/daily
        - name: filename
          value: $(tasks.get-yesterdays-date-and-year.results.date)
        - name: bucket
          value: data.jobtechdev.se-adhoc-test
        - name: mc-host-secret-ref
          value: historical-ads-importer-s3
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - rename-files

    # Convert the ads from the jsonl file to the format we use in OpenSearch.
    - name: convert
      taskRef:
        kind: Task
        name: convert
      params:
        - name: source
          value: $(tasks.get-yesterdays-date-and-year.results.date)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - s3-upload

    # Enrich the ads with JobAd Enrichments.
    - name: enrich
      taskRef:
        kind: Task
        name: enrich
      timeout: 10h0m0s
      params:
        - name: source
          value: $(tasks.get-yesterdays-date-and-year.results.date)
        - name: year
          value: $(tasks.get-yesterdays-date-and-year.results.year)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - convert

    # Write the ads to a new index in OpenSearch.
    - name: write-ads
      taskRef:
        kind: Task
        name: write-ads
      params:
        - name: source
          value: $(tasks.get-yesterdays-date-and-year.results.date)
        - name: today
          value: $(tasks.get-yesterdays-date-and-year.results.date_today)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - enrich

    # Update the alias in OpenSearch to use the new index.
    - name: refresh-alias
      taskRef:
        kind: Task
        name: refresh-alias
      params:
        - name: source
          value: $(tasks.get-yesterdays-date-and-year.results.date)
      workspaces:
        - name: archived_ads
          workspace: shared-workspace-historical-importer
      runAfter:
        - write-ads

  workspaces:
    - name: shared-workspace-historical-importer
